function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlVarenTekstZSmejkoti(sporocilo) {
  // spremeni vse posebne znake v HTML značke. npr.: '<' v &lt
  var enostavniTekst = $('<textarea/>').text(sporocilo).html();

  var tabelaSmejkotov = [
    ["\\;\\)",  "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png' alt='wink.png'>"],
    ["\\:\\)",  "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png' alt='smiley.png'>"],
    ["\\(y\\)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png' alt='like.png'>"],
    [":\\*",  "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png' alt='kiss.png'>"],
    [":\\(",  "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png' alt='sad.png'>"]
  ];

  // Popravek prvotne izvedbe zaradi napačne interpretacije naloge - na forumu sem izvedel, da smejkoti lahko nastopajo 
  // tudi kot podnizi, na kateremkoli mestu: https://ucilnica.fri.uni-lj.si/mod/forum/discuss.php?d=12282
  //      Odg: Vprašanja glede 2.DN
  //      od Dejan Lavbič - četrtek, 30. oktober 2014, 18:55

  for(var i = 0; i < tabelaSmejkotov.length; i++) {
    var regex = RegExp(tabelaSmejkotov[i][0], "gi");
    enostavniTekst = enostavniTekst.replace(regex, tabelaSmejkotov[i][1]);
  }

  return $('<div style="font-weight: bold"></div>').html(enostavniTekst);

}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var kanal = $('#kanal').text();
    var indexKanala = kanal.indexOf("@")+2;
    kanal = kanal.substring(indexKanala, kanal.length);
    sporocilo = cenzurirajBesede(sporocilo);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(divElementHtmlVarenTekstZSmejkoti(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// pred začetkom naložimo seznam prepovedanih besed v tabelo swearWords.
var swearWords;
$.ajax({
    async: false,
    url:"swearWords.csv"
}).done(function(atr){
    swearWords = atr.split(",");
});
 
function nizZvezdic(dolzina) {
  var niz = ""; 
  for(var j = 0; j < dolzina; j++) {
      niz = niz.concat("*");
  }
  return niz;
}

function cenzurirajBesede(sporocilo) {
  for(var i = 0; i < swearWords.length; i++) {
    // \bfag\b stestirano na http://regex101.com
    var re = new RegExp("\\b" + swearWords[i] + "\\b", "gi");
    sporocilo = sporocilo.replace(re, nizZvezdic(swearWords[i].length));
  }

  return sporocilo;
}

var socket = io.connect();


$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('zasebno', function(odgovor){
    $('#sporocila').append(divElementHtmlVarenTekstZSmejkoti(odgovor.sporocilo));
  });

  socket.on('pridruzitevZaklenjenKanal', function(res) {
    $('#sporocila').append(divElementEnostavniTekst(res.odgovor));
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      
      var nizUporabnikaAtKanal = $('#kanal').text().split(" ");
      nizUporabnikaAtKanal[0] = rezultat.vzdevek;
      nizUporabnikaAtKanal = nizUporabnikaAtKanal.join(" ");
      $('#kanal').text(nizUporabnikaAtKanal);

    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
   
    var nizUporabnikaAtKanal = $('#kanal').text().split(" ");
    var indexAt = nizUporabnikaAtKanal.indexOf("@");
    if(indexAt != -1) {
      nizUporabnikaAtKanal[indexAt+1] = rezultat.kanal;
    } else {
      nizUporabnikaAtKanal.push("@");
      nizUporabnikaAtKanal.push(rezultat.kanal);
    }
    nizUporabnikaAtKanal = nizUporabnikaAtKanal.join(" ");
    $('#kanal').text(nizUporabnikaAtKanal);


    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('seznamUporabnikov', function(podatki) {
    $('#seznam-uporabnikov').empty();
    var tabelaUporabnikov = podatki.uporabniki;
    for(var uid in tabelaUporabnikov){
      //console.log(tabelaUporabnikov[uid]);
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(tabelaUporabnikov[uid]));
    }

  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementHtmlVarenTekstZSmejkoti(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});